package com.example.al.retrofit;

import androidx.annotation.NonNull;

public class Jwt {
    public String HTTP_AUTHORIZATION;
    public String HTTP_X_AUTHORIZATION;

    @NonNull
    @Override
    public String toString() {
        return String.format("%s\n%s", HTTP_AUTHORIZATION, HTTP_X_AUTHORIZATION);
    }
}
