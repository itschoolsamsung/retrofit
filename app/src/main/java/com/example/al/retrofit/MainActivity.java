package com.example.al.retrofit;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends Activity implements View.OnClickListener {

	final String MSG_LOADING = "Loading...";
	final String MSG_ERROR = "Error...";
	final String MSG_EMPTY = "Empty login/pass";

	public String token = "";
	EditText editUser;
	EditText editPass;
	EditText editTextMessage;
	EditText editTextShow;

	private Retrofit retrofit = new Retrofit.Builder()
			.baseUrl("http://samsung.avalonstudio.ru")
			.addConverterFactory(GsonConverterFactory.create())
			.build();

	private Request request = retrofit.create(Request.class);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		editUser = findViewById(R.id.editTextUser);
		editPass = findViewById(R.id.editTextPass);
        editTextMessage = findViewById(R.id.editTextMessage);
		editTextShow = findViewById(R.id.editTextShow);

		findViewById(R.id.buttonJwt).setOnClickListener(this);
		findViewById(R.id.buttonAuth).setOnClickListener(this);
		findViewById(R.id.buttonPost).setOnClickListener(this);
		findViewById(R.id.buttonShowAll).setOnClickListener(this);
		findViewById(R.id.buttonShowOne).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.buttonJwt:
				editTextShow.setText(MSG_LOADING);

				Call<Jwt> jwt = request.getJwt("qwe");

				jwt.enqueue(new Callback<Jwt>() {
					@Override
					public void onResponse(Call<Jwt> call, Response<Jwt> response) {
						editTextShow.setText(response.body().toString());
					}

					@Override
					public void onFailure(Call<Jwt> call, Throwable t) {
						editTextShow.setText(MSG_ERROR + "\n" + t.getMessage());
					}
				});

				break;

            case R.id.buttonAuth: //login
                editTextShow.setText(MSG_LOADING);

                String username = editUser.getText().toString();
                String password = editPass.getText().toString();

                if (username.equals("") || password.equals("")) {
                    editTextShow.setText(MSG_EMPTY);
                    return;
                }

                Call<Auth> call = request.getToken(username, password);

                call.enqueue(new Callback<Auth>() {
                    @Override
                    public void onResponse(Call<Auth> call, Response<Auth> response) {
                        token = response.body().token;
                        editTextShow.setText(token);
                    }

                    @Override
                    public void onFailure(Call<Auth> call, Throwable t) {
                        editTextShow.setText(MSG_ERROR + "\n" + t.getMessage());
                    }
                });

                break;

            case R.id.buttonPost: //post message
                editTextShow.setText(MSG_LOADING);

                Call<Result> callResult = request.addMessage(token, editTextMessage.getText().toString());
                callResult.enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {
                        editTextShow.setText("done");
                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {
                        editTextShow.setText(MSG_ERROR + "\n" + t.getMessage());
                    }
                });

                break;

			case R.id.buttonShowAll: //show all messages
				editTextShow.setText(MSG_LOADING);

				Call<Messages> callMessages = request.getMessages(token);
				callMessages.enqueue(new Callback<Messages>() {
					@Override
					public void onResponse(Call<Messages> call, Response<Messages> response) {
						editTextShow.setText(response.body().toString());
					}

					@Override
					public void onFailure(Call<Messages> call, Throwable t) {
						editTextShow.setText(MSG_ERROR + "\n" + t.getMessage());
					}
				});

				break;

			case R.id.buttonShowOne: //show one message
				editTextShow.setText(MSG_LOADING);

				Call<Message> callMessage = request.getMessage(token, 1);
				callMessage.enqueue(new Callback<Message>() {
					@Override
					public void onResponse(Call<Message> call, Response<Message> response) {
						editTextShow.setText(response.body().toString());
					}

					@Override
					public void onFailure(Call<Message> call, Throwable t) {
						editTextShow.setText(MSG_ERROR + "\n" + t.getMessage());
					}
				});

				break;
		}
	}
}
