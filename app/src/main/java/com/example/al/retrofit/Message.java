package com.example.al.retrofit;

public class Message {
	public int id;
	public int id_user;
	public String text;
	public int created_at;
	public int updated_at;
	public String userUsername;
	public String userName;
	public String userEmail;
	public String dateStr;

	@Override
	public String toString() {
		return String.format("%s %s\n%s", dateStr, userName, text);
	}
}
