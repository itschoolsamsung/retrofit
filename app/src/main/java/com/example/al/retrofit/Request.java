package com.example.al.retrofit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Request {
	boolean DEBUG = true;

	@GET("/api/jwt")
	Call<Jwt> getJwt(@Header( DEBUG ? "X-Authorization" : "Authorization" ) String token);

	@GET("/api/message/view")
	Call<Message> getMessage(@Query("token") String token, @Query("id") int id);

	@GET("/api/message/index")
	Call<Messages> getMessages(@Query("token") String token);

	@FormUrlEncoded
	@POST("/api/user/auth")
	Call<Auth> getToken(@Field("username") String username, @Field("password") String password);
//	Call<Auth> getToken(@Body AuthForm authForm);

	@FormUrlEncoded
	@POST("/api/message/add")
	Call<Result> addMessage(@Field("token") String token, @Field("text") String text);

//	@GET("/json.php")
//	Call<Persons> getPersons();
//
//	@GET("/json.php")
//	Call<Person> getPersonById(@Query("id") int id);

//	@GET("/{code}/json.php")
//	Call<Person> getPersonByCode(@Path("code") String code);

//	@POST("/json.php")
//	PostResponse postPerson(@Body Person person);
//
//	public class PostResponse {
//		public String result;
//	}
}
