package com.example.al.retrofit;

import java.util.ArrayList;
import java.util.Iterator;

public class Messages {
	ArrayList<Message> messages;

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();

		Iterator i = messages.iterator();
		while (i.hasNext()) {
			Message message = (Message)i.next();
			result.append(message.toString()).append("\n\n");
		}

		return result.toString();
	}
}
